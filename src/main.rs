extern crate dirs;
extern crate serde;

use copy_tool::Duplicator;

fn main() {
    let duplicator = Duplicator::new();

    if let Some(mut doc_dir) = dirs::document_dir() {
        doc_dir.push("copy_info.json");

        let doc_dir = &doc_dir.into_os_string().into_string().unwrap();

        duplicator.execute(doc_dir);
    } else {
        println!("\nCould not get the document path!");
    }
}
