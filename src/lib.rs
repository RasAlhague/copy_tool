#![deny(clippy::pedantic)]
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;
use structopt::StructOpt;

#[derive(Serialize, Deserialize, Clone)]
struct CopyInfo {
    source: String,
    destination: String,
    identifier: String,
}

impl CopyInfo {
    pub fn new(identifier: &str, source: &str, destination: &str) -> CopyInfo {
        CopyInfo {
            identifier: String::from(identifier),
            source: String::from(source),
            destination: String::from(destination),
        }
    }

    pub fn serialize_muliple(infos: &[CopyInfo], path: &str) -> Result<(), String> {
        let mut file = match File::create(path) {
            Ok(f) => f,
            Err(err) => return Err(format!("Could not create file: {}", err)),
        };
        file.write_all(serde_json::to_string(infos).unwrap().as_bytes())
            .unwrap();

        Ok(())
    }

    pub fn deserialize_muliple(path: &str) -> Result<Vec<CopyInfo>, String> {
        let mut file = match File::open(path) {
            Ok(f) => f,
            Err(err) => return Err(format!("Could not open file: {}", err)),
        };
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();

        let infos: Vec<CopyInfo> = match serde_json::from_str(contents.as_str()) {
            Ok(x) => x,
            Err(e) => {
                println!("Failed to load copy infos: {}", e);

                return Err(String::from("Could not load copy infos."));
            }
        };

        Ok(infos)
    }
}

#[derive(StructOpt)]
#[structopt(about = "A program for copying files into an specific directory.")]
pub enum Duplicator {
    #[structopt(help = "Adds an new entry for copying.")]
    Add {
        #[structopt(short, long)]
        source: String,
        #[structopt(short, long)]
        destination: String,
        #[structopt(short, long)]
        identifier: String,
    },
    #[structopt(help = "Removes an entry for copying.")]
    Remove {
        #[structopt(short, long)]
        identifier: String,
    },
    #[structopt(help = "Changes an entry for copying.")]
    Change {
        #[structopt(short, long)]
        source: String,
        #[structopt(short, long)]
        destination: String,
        #[structopt(short, long)]
        identifier: String,
    },
    #[structopt(help = "Runs the copy_tool.")]
    Run,
}

impl Duplicator {
    #[must_use]
    pub fn new() -> Duplicator {
        Duplicator::from_args()
    }

    pub fn execute(self, path: &str) {
        match self {
            Duplicator::Add {
                identifier,
                source,
                destination,
            } => Duplicator::add(path, &identifier, &source, &destination),
            Duplicator::Change {
                identifier,
                source,
                destination,
            } => Duplicator::change(path, &identifier, &source, &destination),
            Duplicator::Remove { identifier } => Duplicator::remove(path, &identifier),
            Duplicator::Run => Duplicator::run(path),
        }
    }
}

impl Duplicator {
    fn change(path: &str, identifier: &str, source: &str, destination: &str) {
        match CopyInfo::deserialize_muliple(path) {
            Ok(mut copy_infos) => {
                if let Some((pos, _)) = copy_infos
                    .iter()
                    .find_position(|ci| ci.identifier == identifier)
                {
                    copy_infos.remove(pos);

                    let copy_info = CopyInfo::new(identifier, source, destination);

                    Self::write_copy_info(
                        copy_infos,
                        copy_info,
                        path,
                        "Successfully changed entry!",
                    );
                } else {
                    println!("No entry with identifier {} found!", identifier);
                }
            }
            Err(err) => {
                println!("Couldn't read the copy infos! Err: {}", err);
            }
        };
    }

    fn add(path: &str, identifier: &str, source: &str, destination: &str) {
        match CopyInfo::deserialize_muliple(path) {
            Ok(copy_infos) => {
                if copy_infos.iter().any(|ci| ci.identifier == identifier) {
                    println!("Entry for identifier {} already exists!", identifier);
                } else {
                    let copy_info = CopyInfo::new(identifier, source, destination);

                    Self::write_copy_info(
                        copy_infos,
                        copy_info,
                        path,
                        "Successfully added a new entry!",
                    );
                }
            }
            Err(err) => {
                println!("Couldn't read the copy infos! Err: {}", err);
            }
        };
    }

    fn remove(path: &str, identifier: &str) {
        match CopyInfo::deserialize_muliple(path) {
            Ok(mut copy_infos) => {
                if let Some((pos, _)) = copy_infos
                    .iter()
                    .find_position(|ci| ci.identifier == identifier)
                {
                    copy_infos.remove(pos);

                    if let Err(err) = CopyInfo::serialize_muliple(&copy_infos, path) {
                        println!("Could not update copy info file: {}", err);
                    } else {
                        println!("Removed entry from list succesfully!");
                    }
                } else {
                    println!("No entry with identifier {} found!", identifier);
                }
            }
            Err(err) => {
                println!("Couldn't read the copy infos! Err: {}", err);
            }
        };
    }

    fn run(path: &str) {
        match CopyInfo::deserialize_muliple(path) {
            Ok(copy_infos) => {
                for info in copy_infos {
                    println!("Coping file from {} to {};", info.source, info.destination);

                    if let Err(err) = std::fs::copy(&info.source, &info.destination) {
                        println!("Could not copy file! Error: {}", err);
                    } else {
                        println!("Successfully copied file!");
                    }
                }
            }
            Err(err) => {
                println!("Couldn't read the copy infos! Err: {}", err);
            }
        };
    }

    fn write_copy_info(
        mut copy_info_list: Vec<CopyInfo>,
        copy_info: CopyInfo,
        path: &str,
        success_message: &str,
    ) {
        copy_info_list.push(copy_info);

        if let Err(err) = CopyInfo::serialize_muliple(&copy_info_list, path) {
            println!("Could not update copy info file: {}", err);
        } else {
            println!("{}", success_message);
        }
    }
}
